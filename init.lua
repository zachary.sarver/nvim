-- plugin management happens in lua/plugins.lua
require('plugins')
-- 
-- -- convenience definitions 
local o = vim.o -- global option
local wo = vim.wo -- window-local options
local g = vim.g -- global variables
local cmd = vim.cmd -- call commands unexposed to lua

o.termguicolors = true --24-bit color
o.mouse = 'a' -- turn on mouse support in all modes
o.clipboard = [[unnamed,unnamedplus]] --use both the + and * registers
o.expandtab = true -- substitue spaces for tabs
o.completeopt = [[menuone,preview,noselect]] -- show autocompletes, but don't
                                             -- autoselect
o.updatetime = 500 -- autosaves after n ms of inactivity
o.shiftwidth = 4 -- number of spaces for autoindent
o.cursorline = true -- highlight the line the cursor is on

wo.signcolumn = 'auto:3' -- set max width of the "symbol" column
wo.number = true -- line numbering on
wo.relativenumber = true -- relative line numbering on (useful for multi-line
                         -- commands)

g.mapleader = ' ' -- leader key is SPC
g.floaterm_width = 0.8 -- configure size of floating toggleterm windows
g.floaterm_height = 0.8
g.python3_host_prog = '/opt/miniconda3/bin/python3'

-- keybinding
require("keybindings")

-- sweet statusline
require("statusline")

-- theming
require("theme")

-- terminal shortcuts
require("terminal")

-- language server
require("language_server")

-- testin' them units
require("unit_testing")

-- file tree
require("tree")

-- autoformatting
require("autofmt")

-- autocomplete
require("autocomplete")

-- zettelkasten
require("zk")

-- plugin init
require("bufferline").setup{}
require("nvim-autopairs").setup()
require("neoscroll").setup()
require('gitsigns').setup()
require('diffview').setup{}
require('spellsitter').setup()
