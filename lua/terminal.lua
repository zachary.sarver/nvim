require("toggleterm").setup{
  -- size can be a number or function which is passed the current terminal
  size = function(term)
    if term.direction == "horizontal" then
      return 15
    elseif term.direction == "vertical" then
      return vim.o.columns * 0.4
    end
  end,
  open_mapping = [[<C-\>]],
  hide_numbers = true, -- hide the number column in toggleterm buffers
  shade_filetypes = {},
  shade_terminals = true,
  shading_factor = '<number>', -- the degree by which to darken to terminal colour, default: 1 for dark backgrounds, 3 for light
  start_in_insert = true,
  insert_mappings = true, -- whether or not the open mapping applies in insert mode
  persist_size = true,
  direction = 'horizontal',
  close_on_exit = true, -- close the terminal window when the process exits
  shell = vim.o.shell, -- change the default shell
  -- This field is only relevant if direction is set to 'float'
  float_opts = {
    -- The border key is *almost* the same as 'nvim_open_win'
    -- see :h nvim_open_win for details on borders however
    -- the 'curved' border is a custom border type
    -- not natively supported but implemented in this plugin.
    border = 'curved',
    width = math.floor(vim.o.columns * 0.95),
    height = math.floor(vim.o.columns * 0.9),
    winblend = 3,
    highlights = {
      border = "Normal",
      background = "Normal",
    }
  }
}

local Terminal = require("toggleterm.terminal").Terminal

local M = {}

M.lazygit = Terminal:new({
  cmd = "lazygit",
  dir = "git_dir",
  direction = "float",
})

M.vimpc = Terminal:new({
    cmd = "ncmpcpp",
    direction = "float",
})

M.nnn = Terminal:new({
    cmd = "nnn",
    direction = "float",
})

M.zigbuildrun = Terminal:new({
    cmd = "zig build run",
    direction = "horizontal",
    close_on_exit = false,
})

M.zigbuildtest = Terminal:new({
    cmd = "zig build test",
    direction = "horizontal",
    close_on_exit = false,
})

M.bc = Terminal:new({
    cmd = "bc -l",
    direction = "horizontal",
    close_on_exit = false,
})

return M
