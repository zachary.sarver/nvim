-- This file can be loaded by calling `lua require('plugins')` from your init.vim

return require('packer').startup(function()
    use 'wbthomason/packer.nvim' -- packer itself, for auto-updates
    use {
        'nvim-telescope/telescope.nvim', --provides fuzzy file-finding
        requires = {{'nvim-lua/popup.nvim'}, {'nvim-lua/plenary.nvim'}}
    }
    use {
        'nvim-treesitter/nvim-treesitter', -- lsp-like language features
        run = ':TSUpdate'
    }
    use 'kyazdani42/nvim-tree.lua' -- nice file browser
    use 'tpope/vim-commentary' -- easy line (un)commenting with gc
    use 'vim-test/vim-test' -- test runner
    use 'akinsho/toggleterm.nvim' -- convienient popup terminals
    use 'windwp/nvim-autopairs' -- smartparens
    use 'ziglang/zig.vim' -- oh ziggy, ziggy, ziggy-o, who went wherever I did go
    use 'easymotion/vim-easymotion' -- jump around jump around jump around now
    use 'karb94/neoscroll.nvim' -- nicer scrolling
    use "lukas-reineke/indent-blankline.nvim" -- indentation hinting
    use {
        'akinsho/bufferline.nvim', -- show buffers as tabs
        requires = 'kyazdani42/nvim-web-devicons'
    }
    use {
        'nvim-lualine/lualine.nvim', -- swanky status line
        requires = {'kyazdani42/nvim-web-devicons', opt = true}
    }
    use 'simrat39/symbols-outline.nvim' -- lsp symbol magic
    use {
        'lewis6991/gitsigns.nvim', -- diffs at a glance
        requires = {
            'nvim-lua/plenary.nvim'
        },
    }
    use {
         "folke/todo-comments.nvim", -- search for TODO etc
        requires = "nvim-lua/plenary.nvim",
        config = function()
            require("todo-comments").setup {}
        end
    }
    use { 'sindrets/diffview.nvim', requires = 'nvim-lua/plenary.nvim' } -- make diffs better
    use 'famiu/bufdelete.nvim' -- make it easier to delete buffers
    use 'hrsh7th/vim-vsnip' -- snippet management
    use { -- easy lsp management
        'neovim/nvim-lspconfig',
        'williamboman/nvim-lsp-installer',
    }
    use 'agude/vim-eldar'
    use 'mhinz/vim-startify' -- nice start screen  
    use 'hrsh7th/cmp-nvim-lsp' -- autocompletion stuff
    use 'hrsh7th/cmp-buffer'
    use 'hrsh7th/cmp-path'
    use 'hrsh7th/nvim-cmp'
    use {
        "blackCauldron7/surround.nvim",
        config = function()
            require"surround".setup {mappings_style = "surround"}
        end
    }
    use {'nvim-treesitter/nvim-treesitter'}
    use 'nvie/vim-flake8'
    use {
      -- Optional but recommended
      'nvim-treesitter/nvim-treesitter',
      'lewis6991/spellsitter.nvim',
    }
    use 'tell-k/vim-autopep8'
    use 'sotte/presenting.vim'
    use 'sbdchd/neoformat'
    use {
        'renerocksai/telekasten.nvim', -- zettelkasten
        requires = {
            'renerocksai/calendar-vim'
        }
    }
end)
