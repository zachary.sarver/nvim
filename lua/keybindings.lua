local map = vim.api.nvim_set_keymap
local delmap = vim.api.nvim_del_keymap

local options = {noremap=true, silent=true}

-- fuzzy find
map("n", "<leader>ff", "<cmd>lua require('telescope.builtin').find_files()<cr>", options)
map("n", "<leader>.", "<cmd>lua require('telescope.builtin').file_browser()<cr>", options)
map("n", "<leader>g", "<cmd>lua require('telescope.builtin').live_grep()<cr>", options)
map("n", "<leader>bb", "<cmd>lua require('telescope.builtin').buffers()<cr>", options)
map("n", "<leader>c", "<cmd>lua require('telescope.builtin').commands()<cr>", options)
map("n", "<leader>e", "<cmd>NvimTreeFindFile<cr>", options)

-- buffer navigation
map("n", "<leader>bn", "<cmd>BufferLineCycleNext<cr>", options)
map("n", "<leader>bp", "<cmd>BufferLineCyclePrev<cr>", options)

-- pane navigation
map("n", "<leader>wl", "<cmd>wincmd l<cr>", options)
map("n", "<leader>wh", "<cmd>wincmd h<cr>", options)
map("n", "<leader>wk", "<cmd>wincmd k<cr>", options)
map("n", "<leader>wj", "<cmd>wincmd j<cr>", options)
map("n", "<leader>w-", "<cmd>split<cr>", options)
map("n", "<leader>w/", "<cmd>vsplit<cr>", options)
map("n", "<leader>wd", "<cmd>close<cr>", options) -- delete pane
map("n", "<leader>bd", "<cmd>Bdelete<cr>", options) -- delete pane
map("n", "<leader>wm", "<cmd>only<cr>", options) -- maximize

-- code navigation
map("n", "gd", "<cmd>lua vim.lsp.buf.definition()<cr>", options)
map("n", "gr", "<cmd>lua vim.lsp.buf.references()<cr>", options)
map("n", "gh", "<cmd>lua vim.lsp.buf.hover()<cr>", options)
map("n", "<leader>tn", "<cmd>TestNearest<cr>", options)
map("n", "<leader>tf", "<cmd>TestFile<cr>", options)
map("n", "<leader>tl", "<cmd>TestLast<cr>", options)
map("n", "<leader>tv", "<cmd>TestVisit<cr>", options)
map("n", "<leader>ts", "<cmd>TestSuite<cr>", options)
map("n", "<leader>td", "<cmd>TodoTelescope<cr>", options)
map("n", "s", "<Plug>(easymotion-s2)", {})
map("n", "<leader>pp", "<cmd>Autopep8<cr>", options)
map("n", "<leader>pc", "<cmd>call flake8#Flake8()<cr>", options)

-- lsp management
map("n", "<leader>li", "<cmd>LspInstallInfo<cr>", options)

-- plugin management
map("n", "<leader>ps", "<cmd>PackerSync<cr>", options)

-- terminal shortcuts
map("n", "<leader>gs", "<cmd>lua require('terminal').lazygit:toggle()<cr>", options)
map("n", "<leader>am", "<cmd>lua require('terminal').vimpc:toggle()<cr>", options)
map("n", "<leader>zbr", "<cmd>lua require('terminal').zigbuildrun:toggle()<cr>", options)
map("n", "<leader>zbt", "<cmd>lua require('terminal').zigbuildtest:toggle()<cr>", options)
map("n", "<leader>bc", "<cmd>lua require('terminal').bc:toggle()<cr>", options)

-- file management
map("n", "<leader>fs", "<cmd>w<cr>", options)
map("n", "<leader>fb", "<cmd>lua require('terminal').nnn:toggle()<cr>", options)

-- program management
map("n", "<leader>qq", "<cmd>q<cr>", options)

-- zettelkasten
map("n", "<leader>zf", "<cmd>lua require('telekasten').find_notes()<cr>", options)
map("n", "<leader>zd", "<cmd>lua require('telekasten').find_daily_notes()<cr>", options)
map("n", "<leader>zg", "<cmd>lua require('telekasten').search_notes()<cr>", options)
map("n", "<leader>zz", "<cmd>lua require('telekasten').follow_link()<cr>", options)
map("n", "<leader>z", "<cmd>lua require('telekasten').panel()<cr>", options)
map("n", "<leader>zn", "<cmd>lua require('telekasten').new_note()<cr>", options)
