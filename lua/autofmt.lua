local cmd = vim.cmd

-- black
cmd([[
    let g:neoformat_enabled_python = ['black']
    augroup fmt
    autocmd!
    autocmd BufWritePre * undojoin | Neoformat
    augroup END
]])

-- flake8
cmd([[
    let g:flake8_show_in_gutter=1
]])
