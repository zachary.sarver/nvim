local cmd = vim.cmd

cmd([[
let test#python#runner = 'pytest'
let test#python#pytest#executable = 'python -m pytest --workers auto'
let test#python#pytest#options = '-s'
let test#strategy = 'neovim'
let test#neovim#term_position = "vert"
]])
